#include <stdio.h>
int main() {
    
    //Taking the value
    int num1, num2, swap;
    printf("Enter two numbers: ");
    scanf("%d %d", &num1, &num2);
    
    //swaping the value
    swap = num1;
    num1 = num2;
    num2 = swap;
    
    //printing the output
    printf("The swapped values are:  %d, %d", num1,num2);
    
    return 0;
}
