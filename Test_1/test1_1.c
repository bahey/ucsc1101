#include <stdio.h>
int main() {
    double num1, num2, product;
    printf("Enter two numbers: ");
    scanf("%lf %lf", &num1, &num2);  
 
    // Calculating product
    product = num1 * num2;

    // Result
    printf("%.2lf * %.2lf  = %.2lf", num1, num2, product);
    
    return 0;
}
