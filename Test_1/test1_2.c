#include <stdio.h>

int main() {
    float radius, area;
    printf("Enter the radius: ");
    scanf("%f", &radius);  
 
    // Calculating area
    area = 3.1416* radius * radius;

    // Result area using %.2f
    printf("The area of the circle is = %.2f", area);
    
    return 0;
}
