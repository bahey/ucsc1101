#include<stdio.h>
int main()
{
	int number, remainder, reverseNumber =0;
	//getting the number input
	printf("Enter the numbers here : ");
	scanf("%d", &number);
	
	while(number !=0)
	{
		remainder = number % 10;
		reverseNumber = reverseNumber * 10 + remainder;
		number /=10;
	}
	printf("Reversed numbers are : %d ", reverseNumber);
	return 0;
}
