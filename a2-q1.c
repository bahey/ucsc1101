#include <stdio.h>
#include <stdlib.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main() {
	
	int number;
	
	//getting the input from the number
	printf("Enter a number to check : ");
	scanf("%d", &number);
	if (number == 0)
		printf("The Number is Zero");
	else if (number > 0)
		printf("The Number is : POSIIVE");
	else 
		printf("The Number is : NEGATIVE");
	return 0;
}
