#include<stdio.h>
// Code by: A.R. Abdullah Al Bahey
// Course: Programming and Problem Solving
// Assignment on: Functions
// Student ID: 202007

//Gettting the attendees count
int attendees (int value){
	const int attendees =120;
	const int price =15;
	return attendees - ((value-price)/5*20);
}

//Calculating the Cost
int cost (int value){
	const int def_cost = 500;
	return def_cost + attendees(value)*3;
}

//Calcutlating the Income
int income (int value){
	return attendees(value)*value;
}

//Calculating the Profit
int profit (int value){
	return income(value)- cost(value);
}



int main(){
	
	printf("The table below shows the relationship between the Price and Profit");
	int price, final_profit, max_profit, max_price;
	max_profit = 0;
	
	//Processing the price
	for (price=5; price<=50; price+=5){
		final_profit = profit(price);
		printf("\nPrice: %d \t Profit: %d", price,final_profit );
		
		//Getting the maximum profit
		if(max_profit < final_profit)
		{
			max_profit = final_profit;   
			max_price = price;
		}
	}
	
	//Final Decision Given
	printf("\nAccording to the comparison table, the Maximum profit is Rs.%d and The matching price is Rs. %d", max_profit, max_price);
	return 0;
}



