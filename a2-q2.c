#include <stdio.h>
#include <stdlib.h>

int main() {

	int number;
	//getting the input from the number
	printf("Enter a number to check : ");
	scanf("%d", &number);

	if (number%2 == 1)
		printf("The Number is : ODD");
	else
		printf("The Number is : EVEN");

	return 0;
}
