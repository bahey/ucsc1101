#include <stdio.h> 

main() 
{
  //variable declaration
  int number, inc, count=0;
  
  //getting the input number to check
  printf("*PRIME NUMBER IDENTIFIER*\nEnter a number to check : ");
  scanf("%d", &number);
  
  //Checking weather the number can only be devided by 1 and itself only without a reminder
  for(inc=1; inc<=number; inc++)
  {
  	if (number%inc==0)
	  	{
	  		count++;
		}	
  }
  //Output
  if (count==2)
  {
  	printf("%d is a Prime Number", number);
  }
  else {
  
  	printf("%d is NOT a Prime Number", number);
  }
  return 0;    
}

