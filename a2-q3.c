#include <stdio.h>
#include <stdlib.h>

int main() {

	char character;
	int capital, simple;
	//getting the input from the number
	printf("Enter a the character : ");
	scanf("%c", &character);

    //Evaluating the Capital Letters
    capital=(character == 'A' ||character == 'E' ||character == 'I' ||character == 'O' ||character == 'U');
    //Evaluating the simple letters
    simple=(character == 'a' ||character == 'e' ||character == 'i' ||character == 'o' ||character == 'u');
	if (capital || simple)
		printf("' %c ' is a Vowel Letter", character);
	else
		printf("' %c ' is a Consonant Letter", character);

	return 0;
}
