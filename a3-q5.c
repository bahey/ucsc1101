
#include <stdio.h>
int main() {
    int n, i,j, range;
    printf("Enter an integer: ");
    scanf("%d", &n);
    
	
	for (j = 1; j <= n; j++) 
	{
        printf("\n\n");
    
		for (i = 1; i <= n; i++)
		{
	        printf("%d * %d = %d \n", i, j, j * i);
	    }
	}
	    
	
    return 0;
}
